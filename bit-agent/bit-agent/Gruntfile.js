module.exports = function(grunt) {
	grunt.initConfig({
		compass: {
			dist: {
				options: {
					sassDir: 'front/scss',
					cssDir: 'css'
				},
			},
		},
		cssmin: {
			minify: {
				expand: true,
				cwd: 'css/',
				src: ['**/*.css', '!**/*.min.css'],
				dest: 'css/',
				ext: '.min.css',
				options: {
					noAdvanced: true,
				}
			}
		},
		imagemin: {
			build: {
				files: [{
					expand: true,
					// オリジナル画像のパス
					cwd: 'front/img/',
					// 圧縮する画像
					src: [ '**/*.{gif,jpeg,jpg,png}' ],
					// 圧縮画像を出力するパス
					dest: 'img/'
				}]
			}
		},
		concat: {
			files: {
				src : 'front/js/**/*.js',
				dest: 'js/all.js'
			}
		},
		uglify: {
			dist: {
				files: {
					'js/all.min.js': 'js/all.js'
				}
			}
		},
		connect: {
			livereload: {
				options: {
				port: 9001
				}
			}
		},
		watch: {
			scss: {
				files: ['front/scss/**/*.scss'],
				tasks: ['compass']
			},
			css: {
				files: ['css/all.css'],
				tasks: ['cssmin']
			},
			js: {
				files: ['front/js/**/*.js'],
				tasks: ['concat', 'uglify']
			},

		}
	});

	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-compass');
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.registerTask('default', ['compass', 'concat', 'uglify', 'imagemin', 'cssmin', 'connect', 'watch']);
};
