bit = {};
bit.constants = {
	BREAK_POINT: 768,
	LANG: 'bit_language',
	JP: '1',
	KR: '2',
	SP: 'sp',
	PC: 'pc',
	COOKIE_TIME: 10000000,
	SCROLL_TOP_SPEED: 400,
	SP_VIEWPORT_SET: {
		width: 'device-width',
		initial: '1.0',
		minimum: '1.0',
		maximum: '1.0',
		userScalable: '0'
	},
	TB_VIEWPORT_SET: {
		width: '1280',
		maximum: '1',
		userScalable: '0'
	},
	USER_AGENT: {
		iPhone: 'iPhone',
		iPod: 'iPod',
		iPad: 'iPad',
		Android: 'Android',
		Mobile: 'Mobile',
		AndroidTablet: ['A1_07','SC-01C']
	},
}

bit.lang_jp = {
	partner: {
		title: 'ビジネスパートナー募集',
		title2: 'ビジネスパートナーを募集しています。',
		t1: 'ビットは、システム開発の受託開発、業務支援（SES）事業において、協業して頂けるビジネスパートナーの企業様を随時募集しています。 ご希望の企業様は、当社ビジネスパートナー担当宛に、ご連絡頂ければ幸いです。'
	},
	security: {
		title: '情報セキュリティポリシー',
		t1: '株式会社ビットは、以下に示す情報セキュリティ基本方針を定め、社会からの信頼を常に得られるよう、情報セキュリティに取り組みます。',
		t2: '情報セキュリティに関する法令、国が定める指針、その他の規範を遵守します。',
		t3: '情報セキュリティに関する責任を明確にし、対策を実施するための体制を整備します。',
		t4: '情報セキュリティリスクを識別し、組織的、物理的、人的、技術的に適切な対策を実施します。',
		t5: '情報セキュリティに関する教育、啓発を実施し、全従業員が情報セキュリティリテラシーをもって業務を遂行できるようにします。',
		t6: '情報セキュリティに関する管理体制および取り組みについて点検を実施し、継続的に改善・見直しを行います。',
		t7: '情報セキュリティ基本方針が改正された場合には、当WEBサイトに掲載します。',
		t8: '株式会社ビットは、情報漏えいリスクに対し抜本的、かつ高度な対策を講じることにより、お客さまをはじめ社会からの信頼を常に得られるよう、「情報セキュリティポリシー」を策定しました。 今後はこの「情報セキュリティポリシー」および別掲の「個人情報保護のための行動指針（プライバシーポリシー）」を順守し、さまざまな脅威から情報資産を保護し、かつ適正に取り扱うことにより、情報セキュリティの維持に努めます。'
	},
	policy: {
		title: '個人情報保護方針',
		t1: 'プライバシーポリシー',
		t2: '株式会社ビット（以下当社と称します）は、お客さまに提供します商品・サービスを安心して利用していただくとともに、 事業活動を通じて社会の信頼を確立するために、個人情報保護への取組みは重要と認識し、以下の個人情報保護方針を定めています。',
		t3: '(1) 個人情報保護方針について',
		t4: '1.法令・規範の遵守',
		t5: '当社は、個人情報の取扱いにおいて、個人情報の保護に関する法律および関連する政令、その他の規範を遵守します。',
		t6: '2.個人情報の取得・利用・提供等',
		t7: '当社は、当社における個人情報の取得、利用、提供等に関して諸規則を策定し、これに基づき個人情報を適切に取扱います。',
		t8: '3.情報主体の権利尊重',
		t9: '当社は、当社における個人情報の取得、利用、提供等に関して諸規則を策定し、これに基づき個人情報を適切に当社は、 個人情報に関する情報主体の権利を尊重し、ご本人から自己情報の開示、内容の訂正、追加または削除、消去、 第三者への提供の停止を求められたときは、法令の定めに従いこれに対応します。',
		t10: '4.安全管理措置',
		t11: '当社は、個人情報の正確性および安全性を確保するため、情報セキュリティ対策をはじめとする安全措置を講じ、個人情報への不正アクセス、 または個人情報の紛失、破壊、改竄、漏洩等の予防に努めます。',
		t12: '5.個人情報保護体制の確立',
		t13: '当社は、役員および従業員に個人情報保護の重要性を認識させ、個人情報を適切に保護するための体制ならびに仕組みを確立し、さらにこれを維持し、継続的に改善します。',
		t14: '6. 免責',
		t15: '当社は、法律上必要、または法的命令に応じる、あるいは法的手続きに従うために必要と確信する場合に、個人情報の開示を行うことがあります。',
		t16: '(1) 法令・規範の遵守',
		t17: '当社は、個人情報の取扱いにおいて、個人情報の保護に関する法律および関連する政令、その他の規範を遵守します。',
		t18: '(2) 個人情報の取得・利用・提供等',
		t19: '当社は、当社における個人情報の取得、利用、提供等に関して諸規則を策定し、これに基づき個人情報を適切に取扱います。',
		t20: '(3) 情報主体の権利尊重',
		t21: '当社は、個人情報に関する情報主体の権利を尊重し、ご本人から自己情報の開示、内容の訂正、追加または削除、消去、 第三者への提供の停止を求められたときは、法令の定めに従いこれに対応します。',
		t22: '(4) 安全管理措置',
		t23: '当社は、個人情報の正確性および安全性を確保するため、情報セキュリティ対策をはじめとする安全措置を講じ、個人情報への不正アクセス、 または個人情報の紛失、破壊、改竄、漏洩等の予防に努めます。',
		t24: '(5) 個人情報保護体制の確立',
		t25: '当社は、役員および従業員に個人情報保護の重要性を認識させ、個人情報を適切に保護するための体制ならびに仕組みを確立し、さらにこれを維持し、継続的に改善します。',
		t26: '(6) 免責',
		t27: '当社は、法律上必要、または法的命令に応じる、あるいは法的手続きに従うために必要と確信する場合に、個人情報の開示を行うことがあります。',
		t28: '(2) 適用範囲について',
		t29: 'この「個人情報保護方針」は、当社におけるお客様の個人情報の取扱いを定めるものです。',
		t30: '(3) 個人情報の定義について',
		t31: '当社において、個人情報とは、生存する個人に関する情報であって、当該情報に含まれる組織名、氏名、住所、電話番号、 その他の記述又は個人別に付された番号、記号その他、特定の個人を識別できるもの（当該情報のみでは識別できないが、 他の情報と容易に照合することができ、それにより特定の個人を識別できるものを含みます。）をいうものとします。 さらに、個人を識別できる情報に限らず、個人の身体、財産、職種、肩書き等の属性に関して、事実、判断、評価を表すすべての情報も個人情報に含まれます。 当社では、以下の「個人情報保護方針」に基づき、個人情報について細心の注意と最大限の努力をもって、保護及び管理を行っております。',
		t32: '(4) 個人情報の利用目的について',
		t33: '当社が取得した個人情報は次の利用目的の範囲内で取扱います。なお、取得の際に利用目的を個別に通知、明示した場合はその利用目的によるものとさせていただきます。 お客様・お取引先のみなさまの個人情報の利用目的 ．本社と商品の販売･ サービスに関する情報提供のため。',
		t34: '(5) 個人情報の第三者提供について',
		t35: '当社は取得した個人情報を適切に管理し、あらかじめご本人様の同意を得ることなく第三者に提供することはありません。ただし、次の場合は除きます。',
		t36: '法令にもとづく場合',
		t37: '国もしくは地方公共団体又はその委託を受けたものが、法令の定める事務を遂行することに対して協力の必要がある場合に、 本人の同意を得ることにより当該事務の遂行に支障を及ぼす恐れがあるとき。',
		t38: '生命、身体または財産保護のために必要がある場合に、ご本人の同意を得ることが困難であるとき。',
		t39: '公衆衛生の向上、児童の健全な育成の推進のために特に必要な場合に、ご本人の同意を得ることが困難であるとき。',
		t40: '(6) 個人情報に関する問合せについて',
		t41: 'お客様がご自身の個人情報について確認されたい場合には、当社に問合せください。第三者へのお客様の個人情報の遺漏を防止するため、 お客様ご自身であることが当社にて確認できた場合に限り、当社で保管させていただいておりますお客様の個人情報をお客様にお知らせします。 お客様の個人情報に誤りがあったり、変更があった場合にはお客様からの要請にもとづき第三者によるお客様の個人情報の改竄を防止するため、 お客様ご自身であることが当社にて確認できた場合に限り情報を修正または削除させていただきます。'
	},
	list_msg: '一覧を見る',
	news: {
		1: {
			title: '2018年新年のご挨拶',
			msg: {
				t1: '今年も残すところあと僅かとなりましたが、毎年恒例の忘年会の開催が決まりましたのでお知らせします。',
				t2: '年末のお忙しい時期ですが是非ご参加頂けますようお願いします。',
				t3: '・日時： 12月8日（金）　20時～',
				t4: '・会場： はんあり 新大久保本店',
				t5: '・住所： 東京都新宿区大久保２-31-16',
				t6: '・TEL： 050-5284-1485',
				t7: '・HP ： https://www.hotpepper.jp/strJ001155030/',
				t8: '・会費： 3,000円',

			}
		},
		2: {
			title: 'Webサイトリニューアルのお知らせ',
			msg: {
				t1: '日頃は弊社サービスをご利用いただき、まことにありがとうございます。',
				t2: '弊社Webサイトは、1月6日より、リニューアル致しました。',
				t3: 'お客様によりわかりやすくお知らせしてまいります。'
			}

		},
	},
	notice: {
		1: {
			title: '2017年忘年会のお知らせ',
			msg: {
				t1: '今年も残すところあと僅かとなりましたが、毎年恒例の忘年会の開催が決まりましたのでお知らせします。',
				t2: '年末のお忙しい時期ですが是非ご参加頂けますようお願いします。',
				t3: '・日時： 12月8日（金）　20時～',
				t4: '・会場： はんあり 新大久保本店',
				t5: '・住所： 東京都新宿区大久保２-31-16',
				t6: '・TEL： 050-5284-1485',
				t7: '・HP ： https://www.hotpepper.jp/strJ001155030/',
				t8: '・会費： 3,000円',

			}
		},
		2: {
			title: 'サイト障害のお知らせ',
			msg: {
				t1: '1月9日（火）10時頃 ～ 1月10日（水）10時頃までサーバー移転により、',
				t2: 'サイトに接続出来ない障害が発生いたしました。',
				t3: '皆様には、ご迷惑をお掛けしましたことを、心より深くお詫び申し上げます。',
				t4: 'サイト接続障害については1月10日（水）10時頃には復旧しご利用いただけることを、併せてご報告申し上げます。',
				t5: '今後ともサービスの向上に努めてまいりますので、ご理解賜りますようお願い申し上げます。',
			}

		},
		3: {
			title: 'LINEとカカオトークでお問い合わせ!',
			msg: {
				t1: 'ビットは、LINEとカカオトークでお仕事に関する',
				t2: 'お問い合わせを受け付けています。',
				t3: 'また、最新の案件をLINE・カカオでお知らせしています。',
				t4: '「友だちに追加する」ボタンを押してIDで検索bitagentを入力して',
				t5: '検索ボタンを押して下さい。',
				t6: 'すでに「友だちに追加」をされている方は、画面へ移動します。(スマートフォンでアクセス時)'
			}
		},
		4: {
			title: '資本金増資のお知らせ',
			msg: {
				t1: 'この度、2018年03月1日付けで',
				t2: '資本金の増資を行いましたのでここにお知らせいたします。',
				t3: '増資後の資本金：2,500万円',
				t4: '今回の増資は、今後更なるサービスの向上に努め、',
				t5: '事業拡大を図るための財務基盤の強化を行うものであり、',
				t6: 'より一層みなさまに信頼される企業を目指して参ります。',
				t7: '今後ともご指導ご鞭撻を賜りますよう、何卒よろしくお願い申し上げます。'
			}
		},
		5: {
			title: '平成30年4月1日に「開発センター」をオープン',
			msg: {
				t1: 'より良いサービスをご提案できるよう',
				t2: '「開発センター」をオープンいたしました。'
			}
		},
	},
	menuInfoIcon: 'modal-menu-info--jp',
	header_logo: 'top_jp.png',
	title2: 'ビットエージェント',
	subject: 'フリーランス専門エージェントビット',
	title: '株式会社ビット',
	mainImg: 'top_sp_jp.png',
	menulist: {
		menu1: '会社情報',
		menu2: 'ビジネス',
		menu3: 'リクルート',
		menu4: 'お問い合わせ',
	},
	listView: '一覧をみる',
	businessList: {
		menu1: 'SES事業',
		menu2: '委託開発',
		menu3: '社内向け業務アプリ開発',
		menu4: 'アプリケーション開発・運用保守',
		menu5: 'コンサルティング事業',
		menu6: 'スマートフォンアプリ開発・運用保守',
	},
	footerList: {
		menu1: '個人情報保護方針',
		menu2: '情報セキュリティ基本方針',
		menu3: 'ビジネスパートナー企業募集'
	},
	contact: {
		titleCss: 'contents-main-contact--jp',
		title: 'お問い合わせ',
		text1: 'お電話・FAX・メッセンジャーのお問い合わせ',
		text2: 'TEL　　03-6459-2455 (受付時間 10:00~19:00)',
		text3: 'FAX　　03-6459-2465 (24時間受付)',
		text_add_2: 'LINE ID　　bitagent',
		text_add_3: 'カカオトーク ID　　bitagent',
		text4: 'メールのお問い合わせ',
		text4_1: '以下の形式に合わせて書いてください。',
		text5: '名前を入力してください',
		text6: 'メールアドレスを入力してください',
		text6_1: 'メール形式が正しくありません',
		text7: 'タイトルを入力してください',
		text8: 'メッセージを入力してください',
		submit: '内容を確認してお問い合わせを送る'
	},
	company: {
		title: '会社情報',
		text1: '会社概要',
		text2: '沿革',
		text3: '経営理念',
		text4: 'アクセス',
		text5: '開発センターアクセス',
		ta1: '商号',
		tb1: '株式会社ビット',
		ta2: '設立',
		tb2: '2016年4月12日(平成28年4月12日)',
		ta3: '営業本部',
		tb3: '〒150-0001',
		tb3_2: '東京都渋谷区神宮前1−15−14',
		tb3_3: 'Jardin de LUSEINE 5F',
		tb3_4: 'Tel: 03-6459-2455',
		tb3_5: 'Fax: 03-6459-2465',
		ta4: '資本金',
		tb4: '2500万円',
		ta5: '取締役',
		tb5: '代表取締役 李 潤賢(イ ユンヒョン)',
		ta6: '事業内容',
		tb6: 'SES事業、受託開発、アプリ開発、人材仲介',
		ta7: '稼働人数',
		tb7: '30人程',
		ta8: '取引銀行',
		tb8: '三菱UFJ',
		ta9: '2017.09',
		tb9: '韓国支店設立',
		ta10: '2016.07',
		tb10: '渋谷に移転',
		ta11: '2016.04',
		tb11: '設立(神奈川県大和市)',
		ta13: '今後ますます情報技術の革新により、より豊かに便利に時代は変わります。我々ビットでは、IT分野においてその技術ノウハウと業務ノウハウを蓄積し活かすことで、新たな企業経済活動を、最新技術を駆使することで支えます。そして企業の売上げ拡大、コスト低減、業務効率化を促進し、 より良い社会を創造するために、成果を上げることをお約束します。',
		ta14: '東京都渋谷区神宮前1-15-14 Jardin de LUSEINE 3F',
		ta15: 'JR山手線・原宿 徒歩5分',
		/*0410 company new*/
		ta17: '開発本部',
		tb17: '〒169-0072',
		tb17_2: '東京都新宿区大久保1-17-7',
		tb17_3: 'メゾン豊ビル 5-B',
		ta18: '2018.03',
		tb18: '開発センターオープン',
		ta19 : '東京都新宿区大久保1-17-7 メゾン豊ビル 5-B',
		ta20 : 'JR山手線・新大久保 徒歩8分',
		ta21 : '西武新宿線・西武新宿 徒歩5分'
	},
	business: {
		title: 'ビジネス',
		text1: '業務内容',
		text2: '業務実績',
		text3: 'SES事業',
		text4: '蓄積された技術及びシステム運用経験を活かし顧客システムの企画、開発、運用、戦略確立、BPRなど全般的な情報システムサービス提供',
		text5: '受託開発',
		text6: '豊富な人材の中から、お客さま業務に精通したシステムエンジニアでプロジェクトを構成し、分析からサービス開始ま責任をもって開発します。金融・医療・公共等のシステムまで幅広く対応しています。',
		text7: '社内向け業務アプリ開発',
		text8: 'App Store／Androidマーケット向けに公開せず、社内に限定して配布するエンタープライズ向けアプリの開発サ ービスを提供します。店舗設置型の顧客とのコミュニケーションツールから、各種業界のセールス担当が利用するプレゼンツールまで、豊富なノウハウに基づき、お客様に最適なアプリ開発のご提案を行います。',
		text9: 'アプリケーション開発・運用保守',
		text10: 'JAVA、.NET、PHP、Cの開発・運用保守',
		text11: 'コンサルティング事業',
		text12: 'ウェブゲーム、ネイティブゲーム、動画配信サビース、金融・医療・公共・流通等 ・体系的な方法論を通じてITコンサルティングサービス提供。',
		text13: 'スマートフォンアプリ開発・運用保守',
		text14: 'App Store／Androidマーケット向けのアプリ開発サービスを提供します。 ・スマートフォン／タブレットPCの特性を活かした機能を盛り込みながら、お客様のビジネス上の目標を達成するアプリ開発を行います。プロモーションを目的としたアプリから、PC向け／携帯向けアプリのスマートフォン対応まで幅広く承ります。',
		text15: 'ゲームプログラマー（サーバー、IOS 、ANDROID）',
		text16: 'ゲームディレクター、デザイナー、プランナー支援',
		text17: 'インフラ構築',
		text18: 'EC サイト構築',
		text19: '証券システム',
		text20: '航空会社向け運用保守',
		text21: '移動体通信基地局インテグレーション業務、監視、運用、保守',
		text22: 'コールセンターヘルプデスク',
		text23: '指定機械の導入支援',
		text24: 'その他多数'
	},
	recruit: {
		title: 'リクルート',
		t1: '募集分野',
		t2: 'アプリケーション開発エンジニア',
		t3: 'サーバーサイドエンジニア',
		t4: 'フロントエンジニア',
		t5: 'ネットワーク管理者',
		t6: 'プランナー',
		t7: 'ディレクター',
		t8: 'デザイナー',
		t9: '採用対象',
		t10: '各該当する分野での経験者又は資格保有者',
		t11: '専攻関連学科卒業者又は卒用予定者',
		t12: '関連教育機関卒業予定者',
		t13: '必要な技術',
		t14: 'アプリケーション開発エンジニア',
		t15: '設計、開発、テスト',
		t16: 'サーバーサイドエンジニア',
		t17: 'サーバ設計・構築・管理',
		t18: 'データベース設計・構築・管理・Tuning',
		t19: 'ネットワーク管理者',
		t20: 'ネットワーク設計・構築・管理',
		t21: '提出書類',
		t22: '履歴書及び経歴書',
		t23: '採用フロー',
		t24: '1次:書類選考',
		t25: '2次:面接',
		t26: '書類提出 → 書類選考 → 面接 → 内定通知 → 入社',
		t27: 'その他',
		t28: '社会保険加入、通勤交通費全額補助',
		t29: '勤務時間：10:00～19:00 (内休憩1時間) ※勤務地により多少異なります。',
		t30: '勤務地：本社および都内近郊 ※勤務地により異なります。',
		t31: '様々な技術を学ぶ固有の勉強会・研修あり（ビットスクール毎週末「土日」）',
		t32: '応募方法',
		t33: '応募書類をメールにてご連絡下さい。応募書類到着後、1週間以内にご連絡申し上げます。',
		t34: '株式会社ビット　経営管理部　採用担当係',
		t35: 'E-mail: contact@bit-agent.co.jp',
		t36: 'その他、採用に関するお問い合わせもお気軽にご連絡ください。'

	}

};

bit.lang_kr = {
	partner: {
		title: '비지니스 파트너 기업모집',
		title2: '비지니스 파트너를 모집합니다.',
		t1: 'BIT 시스템 개발의 아웃소싱 업무 지원 (SES) 사업에서 협력 해 주실 비즈니스 파트너 기업을 수시로 모집하고 있습니다. 희망 기업은 비즈니스 파트너 담당자 앞으로 연락 주시면 감사하겠습니다.'
	},
	security : {
		title : '정보보안 세큐리티 기본 정책',
		t1 : '주식회사 BIT는 다음과 같은 정보 보안 기본 방침을 정하고 사회에서 신뢰를 항상 얻을 수 있도록 정보 보안에 임합니다. ',
		t2 : '정보 보안에 관한 법령, 국가가 정한 지침, 기타 규범을 준수합니다. ',
		t3 : '정보 보안에 관한 책임을 명확히하고, 대책을 실시하기위한 체제를 정비합니다. ',
		t4 : '정보 보안 위험을 식별하고 조직적, 물리적, 인적, 기술적으로 적절한 대책을 실시합니다. ',
		t5 : '정보 보안 교육, 계몽을 실시하고 모든 직원이 정보 보안 능력을 가지고 업무를 수행 할 수 있도록합니다. ',
		t6 : '정보 보안 관리 체제 및 노력에 대해 점검을 실시하고 지속적으로 개선 · 재검토를 실시합니다. ',
		t7 : '정보 보안 기본 방침이 개정 된 경우에는 당 WEB 사이트에 게재합니다. ',
		t8 : '주식회사 BIT는 정보 유출 위험에 대해 대책을 강구하여 고객을 비롯한 사회의 신뢰를 항상 얻을 수 있도록 「정보 보안 정책」을 책정했습니다. 앞으로는이 「정보 보안 정책」및 선관위의 개인 정보 보호를 위한 행동 지침 (개인 정보 보호 정책)을 준수하고 다양한 위협으로부터 정보 자산을 보호하고 정보 보안 유지에 노력합니다. '
	},
	policy : {
		title : '개인 정보 보호 정책',
		t1 : '개인 정보 보호 정책',
		t2 : '주식회사BIT (이하 당사라고 칭합니다)은 사업 활동을 통해 사회의 신뢰를 구축하기 위해 개인 정보 보호에 노력을 중요시 인식하고 다음의 개인 정보 보호 방침을 정하고 있습니다. ',
		t3 : '(1) 개인 정보 보호 정책',
		t4 : '1. 법령 · 규범의 준수',
		t5 : '당사는 개인 정보의 취급에 관한 법률 및 관련 법령, 기타 규범을 준수합니다. ',
		t6 : '2. 개인 정보의 취득 · 이용 · 제공 등',
		t7 : '우리는 당사의 개인 정보 수집, 이용, 제공 등에 관하여 규칙을 수립하고 이에 따라 개인 정보를 취급합니다. ',
		t8 : '3. 정보 주체의 권리 존중',
		t9 : '우리는 당사의 개인 정보 수집, 이용, 제공 등에 관하여 제 규칙을 수립하고 이에 따라 개인 정보를 적절히 당사는 개인 정보에 대한 정보 주체의 권리를 존중하고 본인으로부터 자기 정보 공개 내용의 정정, 추가 또는 삭제, 제삼자에게의 제공의 중지를 요구받은 때에는 법령의 규정에 따라 이에 대응합니다. ',
		t10 : '4. 안전 관리 조치',
		t11 : '당사는 개인 정보의 정확성 및 안전성을 확보하기 위해 정보 보안 대책을 비롯한 안전 조치를 강구 개인 정보에 대한 무단 액세스 또는 개인 정보의 분실, 파괴, 변조, 누설 등 예방에 노력합니다. ',
		t12 : '5. 개인 정보 보호 체제의 확립',
		t13 : '우리는 임원 및 직원에게 개인 정보 보호의 중요성을 인식하고 개인 정보를 적절히 보호하기위한 체제 및 구조를 확립하고,이를 유지하고 지속적으로 개선합니다. ',
		t14 : '6. 면책',
		t15 : '우리는 법률 상 필요 또는 법적 명령에 응하거나 법적 절차를 준수하는 데 필요한 확신하는 경우에 개인 정보를 공개 할 수 있습니다. ',
		t16 : '(1) 법령 · 규범의 준수',
		t17 : '당사는 개인 정보의 취급에 관한 법률 및 관련 법령, 기타 규범을 준수합니다. ',
		t18 : '(2) 개인 정보의 취득 · 이용 · 제공 등',
		t19 : '우리는 당사의 개인 정보 수집, 이용, 제공 등에 관하여 제 규칙을 수립하고 이에 따라 개인 정보를 적절히 취급합니다. ',
		t20 : '(3) 정보 주체의 권리 존중',
		t21 : '당사는 개인 정보에 대한 정보 주체의 권리를 존중하고 본인으로부터 자기 정보의 개시, 내용의 정정, 추가 또는 삭제, 제삼자에게의 제공의 중지를 요구 한 경우 법령의 규정에 따라 이에 대응합니다. ',
		t22 : '(4) 안전 관리 조치',
		t23 : '당사는 개인 정보의 정확성 및 안전성을 확보하기 위해 정보 보안 대책을 비롯한 안전 조치를 강구 개인 정보에 대한 무단 액세스 또는 개인 정보의 분실, 파괴, 변조, 누설 등 예방에 노력합니다. ',
		t24 : '(5) 개인 정보 보호 체제의 확립',
		t25 : '우리는 임원 및 직원에게 개인 정보 보호의 중요성을 인식하고 개인 정보를 적절히 보호하기위한 체제 및 구조를 확립하고,이를 유지하고 지속적으로 개선합니다. ',
		t26 : '(6) 면책',
		t27 : '우리는 법률 상 필요 또는 법적 명령에 응하거나 법적 절차를 준수하는 데 필요한 확신하는 경우에 개인 정보를 공개 할 수 있습니다. ',
		t28 : '(2) 적용에 관하여',
		t29 : '이「개인 정보 보호 정책」 은 회사의 개인 정보 취급을 정하는 것입니다. ',
		t30 : '(3) 개인 정보의 정의',
		t31 : '우리에서 개인 정보 란 생존하는 개인에 관한 정보로서 당해 정보에 포함되어있는 조직 이름, 성명, 주소, 전화 번호, 기타 기술 또는 개인별로 부여 된 번호, 기호 기타 특정 개인을 식별 할 수있는 (당해 정보만으로는 식별 할 수 없지만, 다른 정보와 용이하게 조합 할 수 있으며, 그로 인하여 특정 개인을 식별 할 수있는 것을 포함합니다.)을 말하며 있습니다. 또한 개인을 식별 할 수있는 정보뿐만 아니라 개인의 신체, 재산, 직종, 직위 등의 특성에 대해 사실, 판단, 평가를 나타내는 모든 정보도 개인 정보에 포함되어 있습니다. 당사는 다음의 "개인 정보 보호 정책"에 따라 개인 정보를 세심한주의와 최대한의 노력을 가지고 보호 및 관리를 실시하고 있습니다. ',
		t32 : '(4) 개인 정보의 이용 목적에 대해',
		t33 : '우리가 수집 한 개인 정보는 다음의 이용 목적의 범위 내에서 취급합니다. 또한, 취득시 이용 목적을 개별적으로 통지 명시하는 경우는 그 이용 목적에 의한 것으로하겠습니다. 고객 · 거래처 여러분의 개인 정보의 이용 목적. 본사와 제품의 판매 · 서비스에 관한 정보 제공을 위해. ',
		t34 : '(5) 개인 정보의 제삼자 제공에 대하여',
		t35 : '우리는 수집 한 개인 정보를 적절하게 관리하고 미리 본인의 동의없이 제 3 자에게 제공 할 수는 없습니다. 그러나 다음의 경우는 제외합니다. ',
		t36 : '법령에 의거 경우',
		t37 : '국가 또는 지방 공공 단체 또는 그 위탁을받은 것이 법령이 정하는 사무를 수행하는 것에 대해 협력 할 필요가있는 경우에는 본인의 동의를 얻는 것으로 해당 사무의 수행에 지장을 미칠 우려가있을 때. ',
		t38 : '생명, 신체 또는 재산 보호를 위해 필요한 경우에, 본인의 동의를 얻기 어려울 때. ',
		t39 : '공중 위생의 향상, 아동의 건전한 육성 추진을 위해 특히 필요한 경우 본인의 동의를 얻기 어려울 때. ',
		t40 : '(6) 개인 정보에 관한 문의에 대해',
		t41 : '고객이 자신의 개인 정보에 대해 확인하려는 경우에는 당사에 문의하십시오. 제삼자에게 개인 정보의 누락을 방지하기 위해 본인임을 당사에서 확인 된 경우에 한하여 당사에서 보관시켜 드리고 있습니다 고객의 개인 정보를 고객에게 알려 합니다. 고객의 개인 정보에 오류가 있거나 변경이있을 경우에는 귀하의 요청에 의거 제삼자에 의한 개인 정보의 변조를 방지하기 위해 본인임을 당사에서 확인할 수 경우에만 정보를 수정 또는 삭제하겠습니다. ',
	},
	list_msg: '리스트로 이동',
	news: {
		1: {
			title: '2018년 새해인사',
			msg: {
				t1: '망년회 공지사항입니다!^^',
				t2: '바쁘시더라도 꼭 참석해주시면 감사하겠습니다.',
				t3: '・일시： 12月8日（금）　20시～',
				t4: '・장소： 항아리 신오쿠보점',
				t5: '・주소： 도쿄도 신주쿠구 오오쿠보2-31-16',
				t6: '・TEL： 050-5284-1485',
				t7: '・HP ： https://www.hotpepper.jp/strJ001155030/',
				t8: '・회비： 3,000円',

			}
		},
		2: {
			title: '홈페이지 리뉴얼 소식',
			msg: {
				t1: '항상 BIT를 사랑해주셔서 감사합니다.^^',
				t2: '1월6일부터 새로운 소식 공지사항등을',
				t3: '홈페이지에 기재 하겠습니다.',
				t4: '감사합니다!'
			}
		}
	},
	notice: {
		1: {
			title: '2017년 망년회 공지',
			msg: {
				t1: '망년회 공지사항입니다!^^',
				t2: '바쁘시더라도 꼭 참석해주시면 감사하겠습니다.',
				t3: '・일시： 12月8日（금）　20시～',
				t4: '・장소： 항아리 신오쿠보점',
				t5: '・주소： 도쿄도 신주쿠구 오오쿠보2-31-16',
				t6: '・TEL： 050-5284-1485',
				t7: '・HP ： https://www.hotpepper.jp/strJ001155030/',
				t8: '・회비： 3,000円',

			}
		},
		2: {
			title : '사이트 접속 장애 안내',
			msg : {
				t1 : '1월 9일(화) 10시 ~ 1월 10일(수) 10시까지 서버 이전을 통해',
				t2 : '사이트에 접속 할수없는 오류가 발생했습니다. ',
				t3 : '여러분께 불편을 드려 진심으로 사과드립니다. ',
				t4 : '사이트 접속 장애에 대해서는 1월 10일(수) 10시에 복구되었습니다.',
				t5 : '앞으로도 향상 노력하겠습니다. 감사합니다. ',
			}
		},
		3: {
			title: 'LINE과 카카오토크로 문의사항대응!',
			msg: {
				t1: '저희BIT는 LINE과 카카오톡으로',
				t2: '문의사항을 받고있습니다.',
				t3: '그리고 최신정보를 LINE과 카카오톡으로 공지하고 있습니다.',
				t4: '아이디 검색 bitagent를 검색하셔서 친구추가 해주세요!',
				t5: '친구추가후 메세지를 남겨주시면 확인후 바로 답변드리겠습니다.',
				t6: '감사합니다.',
			}
		},
		4: {
			title: '자본금 증자 소식',
			msg: {
				t1: '저희 BIT는 2018년 3월1일',
				t2: '자본금 증자를 하였습니다. ',
				t3: '증자 후 자본금 : 2,500 만엔',
				t4: '이번 증자는 향후 한층 더 서비스 향상에 노력해',
				t5: '사업 확대와 재정기반 강화를 위한것이고',
				t6: '더욱더 여러분에게 신뢰받는 기업을 목표로 하겠습니다.',
				t7: '앞으로도 잘 부탁드립니다.'
			}
		},
		5: {
			title: '개발센터 오픈 소식',
			msg: {
				t1: '더 좋은 서비스를 제공 할 수 있도록',
				t2: '개발센터를 오픈 하였습니다.'
			}
		}
	},
	menuInfoIcon: 'modal-menu-info--ko',
	header_logo: 'top_kr.png',
	title2: '비트 에이젠트',
	subject: '일본IT 취업전문 에이젠트',
	title: '주식회사BIT',
	mainImg: 'top_sp_kr.png',
	menulist: {
		menu1: '회사정보',
		menu2: '비지니스',
		menu3: '리쿠르트',
		menu4: '문의사항',
	},
	listView: '항목을보기',
	businessList: {
		menu1: 'SES사업',
		menu2: '위탁개발',
		menu3: '사내시스템어플리케이션개발',
		menu4: '어플리케이션개발・운영보수',
		menu5: '비지니스 컨설팅사업',
		menu6: '스마트폰 어플리케이션개발・운영보수',
	},
	footerList: {
		menu1: '개인정보보호방침',
		menu2: '기본정보방침',
		menu3: '비지니스파트너 기업모집'
	},
	contact: {
		titleCss: 'contents-main-contact--kr',
		title: '문의 사항',
		text1: '전화・FAX・메신져문의',
		text2: 'TEL　　03-6459-2455 (업무시간 10:00~19:00)',
		text3: 'FAX　　03-6459-2465 (24시간)',
		text_add_2: 'LINE ID　　bitagent',
		text_add_3: '카카오톡 ID　　bitagent',
		text4: '메일 문의사항은 아래의 내용을 적어주십시요.',
		text5: '이름을 입력해주세요',
		text6: '메일주소를 입력해주세요',
		text6_1: '메일형식이 맞지않습니다',
		text7: '제목을 입력해주세요',
		text8: '메세지를 입력해주세요',
		submit: '문의사항에 보내기'
	},
	company: {
		title: '회사정보',
		text1: '회사개요',
		text2: '연혁',
		text3: '경영이념',
		text4: '회사위치',
		text5: '개발센터 위치',
		ta1: '상호',
		tb1: '주식회사BIT',
		ta2: '설립',
		tb2: '2016년4월12일',
		ta3: '영업본부',
		tb3: '〒150-0001',
		tb3_2: '도쿄도 시부야구 진구마에1-15-14',
		tb3_3: 'Jardin de LUSEINE 3F',
		tb3_4: 'Tel: 03-6459-2455',
		tb3_5: 'Fax: 03-6459-2465',
		ta4: '자본금',
		tb4: '2500만엔',
		ta5: '대표',
		tb5: '이 윤현',
		ta6: '사업내용',
		tb6: 'SES사업、위탁개발、어플리개발、인재소개',
		ta7: '사원수',
		tb7: '30명정도',
		ta8: '거래은행',
		tb8: '미쯔비시UFJ',
		ta9: '2017.09',
		tb9: '한국지사 설립',
		ta10: '2016.07',
		tb10: '시부야로 이동',
		ta11: '2016.04',
		tb11: '설립(카나가와켄야마토시)',
		ta13: '정보 기술의 혁신을 통해서 보다 편리한 시대로 변하고 있습니다. 비트는 IT 분야에서 기술 노하우와 업무 노하우를 축적하고 활용함으로써 새로운 기업 경제 활동을 최신 기술을 통한 기업의 매출 확대, 비용 절감, 업무 효율화를 촉진하고 더 나은 사회를 창조하기 위해 성과를 올릴 것을 약속합니다.',
		ta14: '도쿄도 시부야구 진구마에1-15-14 Jardin de LUSEINE 3F',
		ta15: 'JR야마노테선 하라주쿠 도보5分',
		/*0410 update*/
		ta17:'개발본부',
		tb17: '〒169-0072',
		tb17_2:'도쿄도 신주쿠구 오오쿠보 1-17-7',
		tb17_3: '메종유타카빌딩 5-B',
		ta18 : '2018.03',
		tb18 : '개발본부 오픈',
		ta19 : '도쿄도 신주쿠구 오오쿠보 1-17-7 메종유타카빌딩 5-B',
		ta20 : 'JR야마노테선 신오오쿠보 도보8分',
		ta21 : '세이부신주쿠선 세이부신주쿠 도보5分',


	},
	business: {
		title: '비지니스',
		text1: '업무내용',
		text2: '업무실적',
		text3: 'SES사업',
		text4: '축적된 기술 및 시스템 운영 경험을 살려 고객 시스템의 기획, 개발, 운영, 전략 수립, BPR 등 전반적인 정보시스템 서비스 제공',
		text5: '위탁개발',
		text6: '풍부한 인재 중에서 고객 업무에 정통한 시스템 엔지니어로 프로젝트를 구성하고 분석에서 서비스 또는 개발합니다. 금융 · 의료 · 공공 등의 시스템까지 폭넓게 대응하고 있습니다.',
		text7: '사내시스템 어플리개발',
		text8: 'App Store / Android 마켓에 공개하지 않고 사내에 한정하여 배포하는 엔터프라이즈 애플리케이션 개발 서비스를 제공합니다. 고객과의 커뮤니케이션 도구에서 다양한 업계의 영업 담당자가 사용하는 프리젠테이션 도구까지 풍부한 노하우를 바탕으로 고객에게 최적의 애플리케이션 개발을 제안합니다.',
		text9: '응용 프로그램 개발 · 운영 및 유지 보수',
		text10: 'JAVA、.NET、PHP、C개발・운용 보수',
		text11: '컨설팅사업',
		text12: '웹 게임, 금융 · 의료 · 공공 · 유통 등 체계적인 방법론을 통해 IT 컨설팅 서비스 제공.',
		text13: '스마트 폰 앱 개발 · 운용 보수',
		text14: 'App Store / Android 마켓의 앱 개발 서비스를 제공합니다. 스마트 폰 / 태블릿 PC의 특성을 살린 기능을 포함시키면서 고객의 비즈니스 목표를 달성하는 응용 프로그램을 개발합니다. 프로모션을 목적으로 한 응용 프로그램에서 PC 용 / 모바일 용 앱 스마트 폰 대응까지 폭넓게 제공하고 있습니다.',
		text15: '게임 프로그래머 (서버, IOS, ANDROID)',
		text16: '게임 디렉터, 디자이너, 기획자 지원',
		text17: '인프라 구축',
		text18: 'EC 사이트 구축',
		text19: '증권 시스템',
		text20: '항공회사를위한 운영 및 유지 보수',
		text21: '이동 통신 기지국 통합 업무, 모니터링, 운영 및 유지 보수',
		text22: '콜센터 지원',
		text23: '지정 기계의 도입 지원',
		text24: '그밖의 다수'
	},
	recruit: {
		title: '리쿠르트',
		t1: '모집 분야',
		t2: '응용 프로그램 개발 엔지니어',
		t3: '서버 사이드 엔지니어',
		t4: '프론트 엔지니어',
		t5: '네트워크 관리자',
		t6: '플래너',
		t7: '디렉터',
		t8: '디자이너',
		t9: '채용 대상',
		t10: '각 해당 분야의 경험자 또는 자격증 소지자',
		t11: '전공 관련학과 졸업자 또는 졸업예정자',
		t12: '관련 교육기관 졸업 예정자',
		t13: '필요한 기술',
		t14: '응용 프로그램 개발 엔지니어',
		t15: '설계, 개발, 테스트',
		t16: '서버 사이드 엔지니어',
		t17: '서버 설계 · 구축 · 관리',
		t18: '데이터베이스 설계 · 구축 · 관리 · Tuning',
		t19: '네트워크 관리자',
		t20: '네트워크 설계 · 구축 · 관리',
		t21: '제출 서류',
		t22: '이력서 및 경력서',
		t23: '채용 순서',
		t24: '1차 : 서류 전형',
		t25: '2차 : 면접',
		t26: '서류 제출 → 서류 전형 → 면접 → 내정 통지 → 입사',
		t27: '기타',
		t28: '사회 보험 가입, 통근 교통비 전액 보조',
		t29: '근무 시간: 10 : 00 ~ 19 : 00 (휴식 1 시간) ※ 근무지에 따라 다를 수 있습니다.',
		t30: '근무지: 본사 및 도내 근교 ※ 근무지에 따라 다릅니다.',
		t31: '기술 교류회 (비트스쿨 토,일)',
		t32: '응모 방법',
		t33: '응모 서류를 이메일로 연락주십시오. 응모 서류 도착 후 1주일 이내에 연락드립니다.',
		t34: '주식회사 비트 경영 관리부 채용 담당계',
		t35: 'E-mail : contact@bit-agent.co.jp',
		t36: '기타 채용에 관한 문의도 부담없이 연락해주십시오.'

	}



};

var validate = function() {
	var name = $('.js-form_name').val();
	var email = $('.js-form_email').val();
	var title = $('.js-form_title').val();
	var message = $('.js-form_message').val();

	var regExp = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;

	if (!name.length > 0) {
		$('.js-error_name').removeClass('is-hidden');
	}

	if (!email.length > 0) {
		$('.js-error_email').removeClass('is-hidden');
	}

	if (!title.length > 0) {
		$('.js-error_title').removeClass('is-hidden');
	}

	if (!message.length > 0) {
		$('.js-error_message').removeClass('is-hidden');
	}

	if (!email.match(regExp) && email.length > 0) {
		$('.js-error_email-reg').removeClass('is-hidden');
	}

	if (!name.length > 0 || !email.length > 0 || !title.length > 0 || !message.length > 0 || !email.match(regExp) && email.length > 0) {
		return false;
	} else {
		$('.js-form_info').submit();
		alert('登録しました。');
	}


}

var validatePc = function() {
	var name = $('.js-pc-form_name').val();
	var email = $('.js-pc-form_email').val();
	var title = $('.js-pc-form_title').val();
	var message = $('.js-pc-form_message').val();

	var regExp = /^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*.[a-zA-Z]{2,3}$/i;

	if (!name.length > 0) {
		$('.js-pc-error_name').removeClass('is-hidden');
	}

	if (!email.length > 0) {
		$('.js-pc-error_email').removeClass('is-hidden');
	}

	if (!title.length > 0) {
		$('.js-pc-error_title').removeClass('is-hidden');
	}

	if (!message.length > 0) {
		$('.js-pc-error_message').removeClass('is-hidden');
	}

	if (!email.match(regExp) && email.length > 0) {
		$('.js-pc-error_email-reg').removeClass('is-hidden');
	}

	if (!name.length > 0 || !email.length > 0 || !title.length > 0 || !message.length > 0 || !email.match(regExp) && email.length > 0) {
		return false;
	} else {
		$('.js-pc-form_info').submit();
		alert('登録しました。');
	}


}

var hiddenContents = function() {
	$('.js-header').addClass('is-hidden');
	$('.js-contents').addClass('is-hidden');
	$('.js-footer').addClass('is-hidden');
}

var showContents = function() {
	$('.js-header').removeClass('is-hidden');
	$('.js-contents').removeClass('is-hidden');
	$('.js-footer').removeClass('is-hidden');
}

var getWidth = function() {
	'use strict';

	var $win = $(window);

	var body_width = $win.width();
	return body_width;
}

var getHeight = function() {
	'use strict';

	var $win = $(window);

	var body_height = $win.height();
	return body_height;
}

var isScreen = function() {
	'use strict';

	var $win = $(window);

	var body_width = $win.width();
	if (body_width <= bit.constants.BREAK_POINT) {
		return bit.constants.SP;
	} else {
		return bit.constants.PC;
	}
}

var isTablet = function() {

	var tbView = 'width='+bit.constants.TB_VIEWPORT_SET.width +
								',maximum-scale='+bit.constants.TB_VIEWPORT_SET.maximum +
								',user-scalable='+bit.constants.TB_VIEWPORT_SET.userScalable;

	if(navigator.userAgent.indexOf(bit.constants.USER_AGENT.iPad) > 0 ||
		(navigator.userAgent.indexOf(bit.constants.USER_AGENT.Android) > 0 &&
			navigator.userAgent.indexOf(bit.constants.USER_AGENT.Mobile) == -1) ||
			navigator.userAgent.indexOf(bit.constants.USER_AGENT.AndroidTablet[0]) > 0 ||
			navigator.userAgent.indexOf(bit.constants.USER_AGENT.AndroidTablet[1]) > 0) {
			$('#js-viewport').attr('content', tbView);
	}
}

var browserLanguage = function() {
	var ua = window.navigator.userAgent.toLowerCase();
	try {
		// chrome
		if( ua.indexOf( 'chrome' ) != -1 ){
			return ( navigator.languages[0] || navigator.browserLanguage || navigator.language || navigator.userLanguage).substr(0,2);
		}
		// それ以外
		else{
			return ( navigator.browserLanguage || navigator.language || navigator.userLanguage).substr(0,2);
		}
	}
	catch( e ) {
		return undefined;
	}
}

var setCookie = function(cName, cValue, cDay) {
	var expire = new Date();
	expire.setDate(expire.getDate() + cDay);
	cookies = cName + '=' + escape(cValue) + '; path=/ ';
	if(typeof cDay != 'undefined') cookies += ';expires=' + expire.toGMTString() + ';';
	document.cookie = cookies;
}

var getCookie = function(cName) {
	cName = cName + '=';
	var cookieData = document.cookie;
	var start = cookieData.indexOf(cName);
	var cValue = '';
	if(start != -1){
		start += cName.length;
		var end = cookieData.indexOf(';', start);
		if(end == -1)end = cookieData.length;
		cValue = cookieData.substring(start, end);
	}
	return unescape(cValue);
}

var deleteCookie = function(cName) {
    setCookie(cName, '', -1);
}

var clearLanguage = function() {
	$('.js-lang_jp').removeClass('is-active');
	$('.js-lang_kr').removeClass('is-active');
	$('.js-is_korean').addClass('is-hidden');
}

var initLanguage = function() {
	if (getCookie(bit.constants.LANG) == bit.constants.JP) {
		$('#output').html(template(bit.lang_jp));
		setCookie(bit.constants.LANG, bit.constants.JP, bit.constants.COOKIE_TIME);
		$('.js-lang_jp').addClass('is-active');
		$('.js-is_korean').addClass('is-hidden');
	} else if (getCookie(bit.constants.LANG) == bit.constants.KR) {
		$('#output').html(template(bit.lang_kr));
		setCookie(bit.constants.LANG, bit.constants.KR, bit.constants.COOKIE_TIME);
		$('.js-lang_kr').addClass('is-active');
		$('.js-is_korean').removeClass('is-hidden');
	} else {
		if( browserLanguage() == 'ja' ) {
			$('#output').html(template(bit.lang_jp));
			setCookie(bit.constants.LANG, bit.constants.JP, bit.constants.COOKIE_TIME);
			$('.js-is_korean').addClass('is-hidden');
			$('.js-lang_jp').addClass('is-active');
		}
		else {
			$('#output').html(template(bit.lang_kr));
			setCookie(bit.constants.LANG, bit.constants.KR, bit.constants.COOKIE_TIME);
			$('.js-is_korean').removeClass('is-hidden');
			$('.js-lang_kr').addClass('is-active');
		}
	}
}

var init = function() {
	clearLanguage();
	initLanguage();
}

$(function() {
	isTablet();
	init();

	$('.js-form_name').focus(function() {
		$('.js-error_name').addClass('is-hidden');
	});

	$('.js-pc-form_name').focus(function() {
		$('.js-pc-error_name').addClass('is-hidden');
	});

	$('.js-form_email').focus(function() {
		$('.js-error_email').addClass('is-hidden');
		$('.js-error_email-reg').addClass('is-hidden');
	});

	$('.js-pc-form_email').focus(function() {
		$('.js-pc-error_email').addClass('is-hidden');
		$('.js-pc-error_email-reg').addClass('is-hidden');
	});

	$('.js-form_title').focus(function() {
		$('.js-error_title').addClass('is-hidden');
	});

	$('.js-pc-form_title').focus(function() {
		$('.js-pc-error_title').addClass('is-hidden');
	});

	$('.js-form_message').focus(function() {
		$('.js-error_message').addClass('is-hidden');
	});

	$('.js-pc-form_message').focus(function() {
		$('.js-pc-error_message').addClass('is-hidden');
	});


	if($('.js-output').hasClass('top') && isScreen() == bit.constants.PC) {
		$('.flexslider').flexslider({
			animation: "slide",
		});
	}

	$(document).on('click', '.js-lang_jp', function() {
		clearLanguage();
		$('#output').html(template(bit.lang_jp));
		$('.js-lang_jp').addClass('is-active');
		$('.js-is_korean').addClass('is-hidden');
		if(isScreen() == bit.constants.SP) {
			$('.js-modal_close').trigger('click');
		}
		setCookie(bit.constants.LANG, bit.constants.JP, bit.constants.COOKIE_TIME);
		if(isScreen() == bit.constants.PC) {
			$('.flexslider').flexslider({
				animation: "slide"
			});
		}
	});

	$(document).on('click', '.js-lang_kr', function() {
		clearLanguage();
		$('#output').html(template(bit.lang_kr));
		$('.js-lang_kr').addClass('is-active');
		$('.js-is_korean').removeClass('is-hidden');
		if(isScreen() == bit.constants.SP) {
			$('.js-modal_close').trigger('click');
		}
		setCookie(bit.constants.LANG, bit.constants.KR, bit.constants.COOKIE_TIME);
		if(isScreen() == bit.constants.PC) {
			$('.flexslider').flexslider({
				animation: "slide"
			});
		}
	});

	$(document).on('click', '.js-modal_close', function() {
		$('.js-modal_menu').css('display', 'none');
		showContents();
		$('.js-modal-menu').css('height', 'auto');
	});

	$(document).on('click', '.js-menu_bt', function() {
		hiddenContents();
		$('.js-modal_menu').css('display', 'block');
	});

	$('#js-company_outline').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $('#js-company_outline-desc').offset().top - 50
		}, 400);
		return false;
	});

	$('#js-company_outline-pc').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $('#js-company_outline-desc-pc').offset().top
		}, 400);
		return false;
	});

	$('#js-company_history').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $('#js-company_history-desc').offset().top - 50
		}, 400);
		return false;
	});

	$('#js-company_history-pc').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $('#js-company_history-desc-pc').offset().top
		}, 400);
		return false;
	});

	$('#js-company_philosophy').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $('#js-company_philosophy-desc').offset().top - 50
		}, 400);
		return false;
	});

	$('#js-company_philosophy-pc').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $('#js-company_philosophy-desc-pc').offset().top
		}, 400);
		return false;
	});

	$('#js-company_access').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $('#js-company_access-desc').offset().top
		}, 400);
		return false;
	});

	$('#js-company_access-pc').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $('#js-company_access-desc-pc').offset().top
		}, 400);
		return false;
	});

	$('#js-business_info').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $('#js-business_info-desc').offset().top - 50
		}, 400);
		return false;
	});

	$('#js-business_info-pc').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $('#js-business_info-desc-pc').offset().top
		}, 400);
		return false;
	});

	$('#js-business_performance').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $('#js-business_performance-desc').offset().top
		}, 400);
		return false;
	});

	$('#js-business_performance-pc').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $('#js-business_performance-desc-pc').offset().top
		}, 400);
		return false;
	});

	$('#js-news').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $('#js-news-desc').offset().top - 50
		}, 400);
		return false;
	});

	$('#js-notice').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $('#js-notice-desc').offset().top
		}, 400);
		return false;
	});

	$('#js-news-pc').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $('#js-news-desc-pc').offset().top
		}, 400);
		return false;
	});

	$('#js-notice-pc').click(function(e) {
		e.preventDefault();
		$('html, body').animate({
			scrollTop: $('#js-notice-desc-pc').offset().top
		}, 400);
		return false;
	});


	$('a[href^="#"].js-page_top').click(function () {
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$('body,html').animate({scrollTop:position}, 400, 'swing');
		return false;
	});

	$('a[href^="#"].js-pc-page_top').click(function () {
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$('body,html').animate({scrollTop:position}, 400, 'swing');
		return false;
	});

	$(window).scroll(function () {
		if(isScreen() == bit.constants.SP) {
			var $this = $(this);
			if ($this.scrollTop() > 300) {
				$('.js-page_top').css('display', 'block');
			} else {
				$('.js-page_top').css('display', 'none');
			}
		}
		if (isScreen() == bit.constants.PC) {
			var $this = $(this);
			if ($this.scrollTop() > 600) {
				$('.js-pc-page_top').css('display', 'block');
			} else {
				$('.js-pc-page_top').css('display', 'none');
			}
		}

	});

	$(window).resize(function() {
		if(isScreen() == bit.constants.PC) {
			if($('.js-output').hasClass('top')) {
				$('.flexslider').flexslider({
					animation: "slide",
				});
			}
		}

	});

});
