﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Root : MonoBehaviour {

    #region [변수]

    public GameObject _body;
    public GameObject _head;
    public GameObject _panel;
    public Transform _canvas;

    public SceneController _sceneController;
    public ButtonController _button_controller;

    public VideoPlay _video;

    private string _mainScene;

    #endregion

    public Transform Canvas
    {
        set { _canvas = value; }
        get { return _canvas; }
    }

    public GameObject Head
    {
        set { _head = value; }
        get { return _head; }
    }

    public GameObject Body
    {
        set { _body = value; }
        get { return _body; }
    }

    public GameObject Panel
    {
        set { _panel = value; }
        get { return _panel; }
    }

    public string StartScene
    {
        set { _mainScene = value; }
        get { return _mainScene; }
    }

    // Use this for initialization
    void Start()
    {

    }

    public void Init(string startscene)
    {
        var controller = KUtill.CreateGameObject("Prefabs/Controller/SceneController", _canvas).GetComponent<SceneController>();
        controller.Init(startscene);

        _button_controller = KUtill.CreateGameObject("Prefabs/Controller/ButtonController").GetComponent<ButtonController>();
        _button_controller._scene_controller = controller;
    }

    void Awake()
    {
        // Setting
        _canvas = KUtill.FindTransform(gameObject, "Canvas");
    }

    // Update is called once per frame
    void Update()
    {

    }
}


