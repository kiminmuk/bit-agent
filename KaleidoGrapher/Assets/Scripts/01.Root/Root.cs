﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Root : MonoBehaviour {

    public GameObject _root;

    public GameDataManager _GameDataManager;
    public WebManager _webManager;
    public ButtonController _button_controller;

    public string StartScene;
    // todo : test 
       
    private void Awake()
    {
        // root link
        _root = gameObject;

        // Create Manager
        _webManager = KUtill.CreateGameObject("Prefabs/Manager/WebManager").GetComponent<WebManager>();
        //_GameDataManager = KUtill.CreateGameObject("Prefabs/Manager/GameDataManager").GetComponent<GameDataManager>();
        //_UIManager = KUtill.CreateGameObject("Prefabs/Manager/UIManager").GetComponent<UIManager>();

        //// UI Root Create
        //var uiroot = KUtill.CreateGameObject("Prefabs/UI_Root");
        //uiroot.name = "UI_Root";

        StartCoroutine(CreateManager());
   
        
    }

    IEnumerator CreateManager()
    {
        _GameDataManager = KUtill.CreateGameObject("Prefabs/Manager/GameDataManager").GetComponent<GameDataManager>();

        if (!_GameDataManager.Complate)
            yield return null;
      
        var uiroot = KUtill.CreateGameObject("Prefabs/UI_Root").GetComponent<UI_Root>();
        uiroot.Init(StartScene);

       
        
        //_UIManager.UIRoot = uiroot;

        //_UIManager._mainScene = MainScene;

        //_UIManager.SetMainPage();

    }


}
