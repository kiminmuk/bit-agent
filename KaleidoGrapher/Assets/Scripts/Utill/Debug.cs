﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Debug
{
    public static bool _use = true;


	public static void Log(string str)
    {
        if (_use)
            UnityEngine.Debug.Log(str);
    }

    public static void LogWarning(object message)
    {
        if (_use)
            UnityEngine.Debug.LogWarning(message);
    }
}
