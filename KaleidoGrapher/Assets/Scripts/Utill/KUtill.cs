﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KUtill : MonoBehaviour
{
    public delegate void Ref_del<T>(T item, ref T info);

    public static GameObject GerResourcesPrefab(string path)
    {
        var obj = Resources.Load(path) as GameObject;
        return obj;
    }

    public static void Sort<T>(ref T t1, ref T t2)
    {
        var temp = t1;
        t1 = t2;
        t2 = temp;
    }

    #region [Object Create]

    public static GameObject CreateGameObject(GameObject obj)
    {
        var ins = Instantiate(obj);

        ins.transform.position = Vector3.zero;
        ins.transform.localPosition = Vector3.zero;

        return obj;
    }

    public static GameObject CreateGameObject(string path)
    {

        var obj = Resources.Load(path) as GameObject;
        var ins = Instantiate(obj);

        ins.name = obj.name;
        ins.transform.position = Vector3.zero;
        ins.transform.localPosition = Vector3.zero;

        return ins;
    }

    public static GameObject CreateGameObject(string path, Transform transform)
    {
        var obj = Resources.Load(path) as GameObject;
        var ins = Instantiate(obj);

        ins.name = obj.name;
        ins.transform.SetParent(transform);
        ins.transform.position = Vector3.zero;
        ins.transform.localPosition = Vector3.zero;

        return ins;
    }

    public static GameObject CreateGameObject(string path, Transform transform, float x, float y)
    {
        var obj = Resources.Load(path) as GameObject;
        var ins = Instantiate(obj);

        ins.transform.SetParent(transform);
        Vector3 pos = new Vector3(x, y, 0);
        ins.transform.position = pos;
        ins.transform.localPosition = pos;

        return ins;
    }

    public static GameObject CreateGameObject(string path, Transform transform, Vector3 pos)
    {
        var obj = Resources.Load(path) as GameObject;
        var ins = Instantiate(obj);

        ins.transform.SetParent(transform);
        ins.transform.position = pos;
        ins.transform.localPosition = pos;

        return ins;
    }

    // 생성후 데이터 넣을때 사용
    public static GameObject CreateGameObject<T>(string path, Transform transform, Ref_del<T> refdel, T item)
    {
        var obj = Resources.Load(path) as GameObject;
        var ins = Instantiate(obj);

        ins.transform.SetParent(transform);
        ins.transform.position = Vector3.zero;
        ins.transform.localPosition = Vector3.zero;

        var temple = ins.GetComponent<T>();
        refdel(item, ref temple);

        return ins;
    }
    #endregion
    
    public static void ProcessStart(string path)
    {
        System.Diagnostics.Process.Start(path);
    }
    
    public static bool CheckDictionary<T>(string key, Dictionary<string, T> dic)
    {
        
        return false;
    }


    public static T GetDictionaryValue<T>(string key, Dictionary<string, T> dic)
    {
        T value;
        var check = dic.TryGetValue(key, out value);
        return value;
    }

    #region 기본
        
    // Recursive
    public static Transform FindTransform(Transform parentTM, string objName)
    {
        if (parentTM == null) return null;

        foreach (Transform trans in parentTM)
        {
            if (trans.name == objName)
            {
                return trans;
            }

            Transform foundTransform = FindTransform(trans.gameObject, objName);
            if (foundTransform != null)
            {
                return foundTransform;
            }
        }

        return null;
    }
    public static Transform FindTransform(GameObject parentObj, string objName)
    {
        if (parentObj == null) return null;
        return FindTransform(parentObj.transform, objName);
    }

    public static void WriteLog(string log, params object[] args)
    {
        Debug.Log(string.Format(log, args));
    }

    public static Object[] AllLoadResources(string path)
    {
        //List<T> _list = new List<T>();

        var _list = Resources.LoadAll(path);

        return _list;        
    }

    public static string[] GetResourcesPath(string path)
    {
        var list = Resources.LoadAll(path);

        var paths = new string[list.Length];

        for(int i = 0; i < list.Length; i++)
        {
            paths[i] = path + '/' +  list[i].name;
        }

        return paths;
    }

    #endregion
}
