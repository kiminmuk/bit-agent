﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameDataManager : MonoBehaviour
{

    public enum eCategory
    {
        Main,
        Game,
        Movie,
    }

    private static GameDataManager _instance;

    public static GameDataManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType(typeof(GameDataManager)) as GameDataManager;
                if (_instance == null)
                {
                    Debug.Log("Error");
                }
            }

            return _instance;
        }
    }


    public List<GameData> _gameList;

    public List<GameData> GameList
    {
        get { return _gameList; }
    }

    public bool Complate = false;

    GameData _currentData;

    public GameData CurrentData
    {
        set { _currentData = value; }
        get { return _currentData; }
    }

    // todo : 서버에서 가져와야함
    //img url
    //http://13.230.159.206:8000/static/img/Trailer_image/Trailer_Main_01.jpg

    #region [Awake]
    public void Awake()
    {
        Complate = false;

        _gameList = new List<GameData>
        {
            // Main Content Category
            new GameData
            {
                GameName  = "ZELDA",
                DetailText = "_______________が選択されました。準備ができたらSTARTを押してください。終了時にはENDを押してください。",
                GamePath = "C:/Program Files/Starcraft/StarCraft.exe",
                ImageName = "",
                ImageURL = "https://nintendoeverything.com/wp-content/uploads/zelda-breath-wild-3-656x344.jpg",
                PeapleName = "",
                VideoURL = Application.streamingAssetsPath + "/Movies/zelda.mp4",
                Category = 1
            },

            new GameData
            {
                GameName  = "FIFA",
                DetailText = "_______________が選択されました。準備ができたらSTARTを押してください。終了時にはENDを押してください。",
                GamePath = "C:/Program Files/Starcraft/StarCraft.exe",
                ImageName = "",
                ImageURL = "https://media.contentapi.ea.com/content/dam/ea/easports/fifa/home/2017/june/5/homepage-share.jpg",
                PeapleName = "",
                VideoURL = Application.streamingAssetsPath + "/Movies/fifa.mp4",
                Category = 1
            },

            new GameData
            {
                GameName  = "Witcher",
                DetailText = "_______________が選択されました。準備ができたらSTARTを押してください。終了時にはENDを押してください。",
                GamePath = "C:/Program Files/Starcraft/StarCraft.exe",
                ImageName = "",
                ImageURL = "http://jp.automaton.am/wp-content/uploads/2015/07/the-witcher-3-new-game-plus-mode-details-header-696x435.jpg",
                PeapleName = "",
                VideoURL = Application.streamingAssetsPath + "/Movies/witcher.mp4",
                Category = 1
            },

            // Game Category

            new GameData
            {
                GameName  = "ZELDA",
                DetailText = "_______________が選択されました。準備ができたらSTARTを押してください。終了時にはENDを押してください。",
                GamePath = "C:/Program Files/Starcraft/StarCraft.exe",
                ImageName = "",
                ImageURL = "https://nintendoeverything.com/wp-content/uploads/zelda-breath-wild-3-656x344.jpg",
                PeapleName = "",
                VideoURL = Application.streamingAssetsPath + "/Movies/zelda.mp4",
                Category = 2
            },

            
            new GameData
            {
                GameName  = "FIFA",
                DetailText = "_______________が選択されました。準備ができたらSTARTを押してください。終了時にはENDを押してください。",
                GamePath = "C:/Program Files/Starcraft/StarCraft.exe",
                ImageName = "",
                ImageURL = "https://media.contentapi.ea.com/content/dam/ea/easports/fifa/home/2017/june/5/homepage-share.jpg",
                PeapleName = "",
                VideoURL = Application.streamingAssetsPath + "/Movies/fifa.mp4",
                Category = 2
            },

            new GameData
            {
                GameName  = "Witcher",
                DetailText = "_______________が選択されました。準備ができたらSTARTを押してください。終了時にはENDを押してください。",
                GamePath = "C:/Program Files/Starcraft/StarCraft.exe",
                ImageName = "",
                ImageURL = "http://jp.automaton.am/wp-content/uploads/2015/07/the-witcher-3-new-game-plus-mode-details-header-696x435.jpg",
                PeapleName = "",
                VideoURL = Application.streamingAssetsPath + "/Movies/witcher.mp4",
                Category = 2
            },

            new GameData
            {
                GameName  = "Farcry5",
                DetailText = "_______________が選択されました。準備ができたらSTARTを押してください。終了時にはENDを押してください。",
                GamePath = "C:/Program Files/Starcraft/StarCraft.exe",
                ImageName = "",
                ImageURL = "https://gamecolony.jp/wp-content/uploads/2018/03/screenshot_20180327-1056008361017839679041446-730x410.png",
                PeapleName = "",
                VideoURL = Application.streamingAssetsPath + "/Movies/farcry.mp4",
                Category = 2
            },
        };

        Complate = true;
    }
    #endregion

    #region [function]

    public void AddGameList(GameData GameData)
    {
        _gameList.Add(GameData);
    }

    public List<GameData> GetGameList()
    {
        return _gameList;
    }

    public List<GameData> GetGameList(int category)
    {
        // category별 리스트 리턴

        var list = new List<GameData>();
        for (int i = 0; i < _gameList.Count; i++)
        {
            if (_gameList[i]._categoryCount == category)
                list.Add(_gameList[i]);
        }
        return list;
    }

    public void InputGameData(GameData info, ref GameData g)
    {
        //g._gameName = info._gameName;
        //g._exeRoot = info._exeRoot;
        //g._categoryCount = info._categoryCount;
        //g._detailText = info._detailText;
        //g._imgName = info._imgName;
        //g._imgURL = info._imgURL;        
        //g._firstMin = info._firstMin;
        //g._lastMin = info._lastMin;
        //g._videoURL = info._videoURL;
    }

    public void ClearCurrentData()
    {
        Destroy(_currentData);
    }

    

    #endregion
}
