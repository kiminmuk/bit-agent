﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI_Button_Menu : UI_Button
{

    public string Key;
    public string _string;

    public string Name
    {
        set { _name = value; }
        get { return _name; }
    }

    // Use this for initialization
    protected override void Awake()
    {
        base.Awake();
        _name = gameObject.name;
    }

    protected override void OnClickListner()
    {
        base.OnClickListner();
        //ButtonController.Instance.SetClickEvent(_eButton, SceneKey);
    }  
}

