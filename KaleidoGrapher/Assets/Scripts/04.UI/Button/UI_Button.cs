﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public enum eButtonEvent
{
    None, Video, Detail, Menu, Back, Play
}

public class UI_Button : MonoBehaviour
{
    // data save    
    public Button _button;
    [SerializeField]
    protected string _name;
    [SerializeField]
    protected string _key;

    public GameData _gameData;
    public SceneBase _scene;

    public string SceneKey;

    public delegate void DelegateFunc<T>(T s);

    //button enum

    public eButtonEvent _eButton;

    protected virtual void Awake()
    {
        _name = "";
        _key = gameObject.name;

        // button event link
        _button = gameObject.GetComponent<Button>();
        _button.onClick.AddListener(OnClickListner);
    }


    protected virtual void Start()
    {
       
    }  

    protected virtual void OnClickListner()
    {
        //switch (_eButton)
        //{
        //    case eButtonEvent.None:
        //        break;
        //    case eButtonEvent.Detail:
        //        ButtonController.Instance.CallOpenDatialInfo(_gameData);
        //        break;
        //    case eButtonEvent.Video:
        //        ButtonController.Instance.CallVideoPlay(_gameData.VideoURL);
        //        break;
        //    case eButtonEvent.Menu:
        //        ButtonController.Instance.CallSceneChange(SceneKey);
        //        break;
        //    case eButtonEvent.Back:
        //        ButtonController.Instance.CallBackButton();
        //        break;
        //    case eButtonEvent.Play:
        //        ButtonController.Instance.CallGamePlay(_gameData.GamePath);
        //        break;  
        //}

        ButtonController.Instance.ButtonEvent(_eButton, this);
    }

}

