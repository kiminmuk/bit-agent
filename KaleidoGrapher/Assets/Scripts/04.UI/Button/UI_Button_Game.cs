﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Button_Game : UI_Button
{   
    
    public static event DelegateFunc<GameData> _onClick;

    private GameData _data;

    public string EventKey;

    protected override void Awake()
    {
        LoadGameData();
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
        if (_gameData != null)
        {
            StartCoroutine(ImageDownload());
        }

    }

    protected override void OnClickListner()
    {
        base.OnClickListner();
        //if(_onClick != null)
        //{
        //    if(_data != null)
        //        _onClick(_data);
        //}
        //ButtonController.Instance.SetClickEvent(_eButton, _gameData);
    }

    public void OnClickButton()
    {

    }

    private void LoadGameData()
    {
        //gamedata load
        var info = gameObject.GetComponent<GameData>();
        if (info == null)
        {
            Debug.Log(gameObject.name + " : info data is null");
            return;
        }
        _gameData = info;
    }

    // img download
    IEnumerator ImageDownload()
    {
        using (WWW www = new WWW(_gameData.ImageURL))
        {
            // wait for download
            Debug.Log(gameObject.name + " : Data DownLoad : ");
            yield return www;

            //texture -> sprite     
            var texture = www.texture;
            var width = texture.width;
            var height = texture.height;

            Debug.Log("Image Download Success : " + gameObject.name);

            Sprite sprite = Sprite.Create(texture, new Rect(0, 0, width, height), Vector2.zero);

            gameObject.GetComponent<Image>().sprite = sprite;
        }
    }

    public void SetDelegateFunc(DelegateFunc<GameData> del, GameData data)
    {
        _onClick = del;
        _data = data;
    }

    
}
