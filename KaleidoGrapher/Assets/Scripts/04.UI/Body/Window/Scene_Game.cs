﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene_Game : SceneBase
{

    #region [변수]
    public GameObject _main_window;
    public GameObject _detail_window;
    public GameObject _layout;

    // image size
    private const int _width = 390;
    private const int _height = 281;

    private const int MAX_X = 3;

    // defalt position
    private float _posX = -955;
    private float _posY = 0;

    #endregion

    #region [Awake]
    protected override void Awake()
    {
        base.Awake();
        //if (UIManager.Instance.CurrentScene != gameObject)
        //    gameObject.SetActive(false);

        CreateImageData();
	}

    #endregion

    #region [Function]
    //이 밑으로는 추후 수정 해야함.

    public void onClick_Button(GameData _data)
    {
        //todo : 가라데이터
        _main_window.SetActive(false);

        if(_detail_window == null)
            _detail_window = _controller.SearchScene("Detail");

        _detail_window.SetActive(true);
        
    }

    public void onClick_Start()
    {
        //System.Diagnostics.Process.Start("C:/Program Files/Starcraft/StarCraft.exe");        
    }

    public void onClick_End()
    {
        _detail_window.SetActive(false);
        _main_window.SetActive(true);        
    }    

    public void OpenDetailWindow(GameData data)
    {
        _main_window.SetActive(false);
        if (_detail_window == null)
            _detail_window = _controller.SearchScene("Detail");       

        _detail_window.GetComponent<Detail_window>().OpenDetailWindow(data);
    }

    
    
    public void CloseDetailWindow()
    {
        _main_window.SetActive(true);
        _detail_window.SetActive(false);
    }

    void CreateImageData()
    {
        var list = GameDataManager.Instance.GetGameList(2);
        var y = _posY;
        var x = _posX;
        for(int i = 0; i < list.Count; i ++)
        {  
            var m = KUtill.CreateGameObject("Prefabs/Game_Prefabs/GameImage", _layout.transform, x, y);
            var info = m.GetComponent<GameData>();
            var g = m.GetComponent<UI_Button_Game>();
            info.SetGameData(list[i]);
            g._scene = this;
            g.SetDelegateFunc(OpenDetailWindow, info);

            x += _width + 30;

            //if ((i+1) % MAX_X == 0)
            //{
            //    y = (float)(y - _height - 10);
            //    x = _posX;
            //}
        }
    }

    public void OnClickButton()
    {

    }

    #endregion
}
