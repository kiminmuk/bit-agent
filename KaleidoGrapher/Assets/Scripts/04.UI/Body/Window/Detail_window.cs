﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Detail_window : SceneBase {

    public GameData _game_info;
    public Scene_Game _sceneGame;

    public Text _gameName;
    public Text _peapleName;
    public Text _firstMin;
    public Text _lastMin;
    public Text _detailText;
    public Button _startbutton;
    public Button _endButton;
    public Image _gameImage;
    public UI_Button_Body _button;

    public bool fin = false;

    private void OnEnable()
    {
        //OpenDetailWindow();
    }

    public void OpenDetailWindow(GameData info)
    {
        fin = false;
        _gameImage.gameObject.SetActive(false);
        _game_info = info;
        gameObject.SetActive(true);
        _gameName.text = _game_info.GameName;
        //_peapleName.text = _game_info.PeapleName;
        //_firstMin.text = "5";
        //_lastMin.text = "10";
        _detailText.text = _game_info.DetailText;

        _button.SetDelegateFunc(onClickFunction, info.VideoURL);
        _button._gameData = _game_info;

        //_gameImage.gameObject.SetActive(false);

        _startbutton.GetComponent<Button>().onClick.AddListener(OnClickStart);
        _endButton.GetComponent<Button>().onClick.AddListener(OnClickEnd);

        StartCoroutine(ImageDownLoad());
    }

    public void OpenDetailWindow()
    {
        fin = false;
        _gameImage.gameObject.SetActive(false);
        //_game_info = GameDataManager.Instance.CurrentData;
        if (_game_info == null)
            return;
        gameObject.SetActive(true);
        _gameName.text = _game_info.GameName;
        //_peapleName.text = _game_info.PeapleName;
        //_firstMin.text = "5";
        //_lastMin.text = "10";
        _detailText.text = _game_info.DetailText;

        //_button.SetDelegateFunc(onClickFunction, _game_info.VideoURL);
        _button._gameData = _game_info;

        //_gameImage.gameObject.SetActive(false);

        _startbutton.GetComponent<Button>().onClick.AddListener(OnClickStart);
        _endButton.GetComponent<Button>().onClick.AddListener(OnClickEnd);

        StartCoroutine(ImageDownLoad());
    }

    IEnumerator ImageDownLoad()
    {
        using (WWW www = new WWW(_game_info.ImageURL))
        {
            // wait for download
            Debug.Log(gameObject.name + " : Data DownLoad : ");
            yield return www;

            //texture -> sprite     
            var texture = www.texture;
            var width = texture.width;
            var height = texture.height;

            Debug.Log("Imagem Download Success : " + gameObject.name);

            Sprite sprite = Sprite.Create(texture, new Rect(0, 0, width, height), Vector2.zero);

            _gameImage.sprite = sprite;
            _gameImage.gameObject.SetActive(true);
            
        }

        
    }

    public void OnClickStart()
    {
        KUtill.ProcessStart(_game_info.GamePath);
    }

    public void OnClickEnd()
    {
        //fin = false;
        //_sceneGame.CloseDetailWindow();
        //gameObject.SetActive(false);
    }

    private void OnDisable()
    {
        OnClickEnd();
    }

    public void onClickFunction(string str)
    {       

    }

}
