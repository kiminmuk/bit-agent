﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneBase : MonoBehaviour {

    protected int _sceneCount;
    protected bool _useScene;
    protected SceneController _controller;
    protected string _sceneName;

    [SerializeField]
    protected string _key;

    public string Name
    {
        set { _sceneName = value; }
        get { return _sceneName; }
    }

    public int Count
    {
        set { _sceneCount = value; }
        get { return _sceneCount; }
    }
    public bool Active
    {
        set { _useScene = value; }
        get { return _useScene; }
    }

    protected virtual void Awake()
    {
        _useScene = false;
        _sceneName = gameObject.name;
        _key = _sceneName;

        //UIManager.Instance.SetSceneBase(_key, this);        
    }

    public virtual void Init(SceneController controller)
    {
        _controller = controller;
    }

    protected virtual void CreateScene()
    {
        
    }


    // todo : 서버 통신 관련
    public virtual void SendToServer()
    {

    }

    public virtual void OpenScene()
    {
        gameObject.SetActive(true);
    }

    public virtual void CloseScene()
    {
        gameObject.SetActive(false);
    }

   
}
