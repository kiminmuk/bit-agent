﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scene_Movie : SceneBase
{
    public GameObject _main_window;
    public GameObject _video_Window;
    public GameObject _layout;

    // image size
    private const int _width = 320;
    private const int _height = 160;

    private const int MAX_X = 4;

    // defalt position
    private float _posX = -480;
    private float _posY = 136;

    protected override void Awake()
    {
        base.Awake();

        CreateImageData();
    }

    #region [Function]
    //이 밑으로는 추후 수정 해야함.

    public void onClick_Button()
    {
        //todo : 가라데이터
        _main_window.SetActive(false);
        _video_Window.SetActive(true);
    }

    public void onClick_Start()
    {
        //System.Diagnostics.Process.Start("C:/Program Files/Starcraft/StarCraft.exe");        
    }

    public void onClick_End()
    {
        _video_Window.SetActive(false);
        _main_window.SetActive(true);
    }

    public void OpenDetailWindow(GameData data)
    {
        _main_window.SetActive(false);
        _video_Window.SetActive(true);
        //_video_Window.GetComponent<Detail_window>().OpenDetailWindow(data);
    }

    public void CloseDetailWindow()
    {
        _main_window.SetActive(true);
    }

    void CreateImageData()
    {
        var list = GameDataManager.Instance.GetGameList(2);
        var y = _posY;
        var x = _posX;
        for (int i = 0; i < list.Count; i++)
        {
            var m = KUtill.CreateGameObject("Prefabs/Game_Scene/Image", _layout.transform, x, y);
            var info = m.GetComponent<GameData>();
            info.SetGameData(list[i]);
            //m.GetComponent<UI_Button>()._gameScene = this;

            x += _width;

            if ((i + 1) % MAX_X == 0)
            {
                y = (float)(y - _height - 10);
                x = _posX;
            }
        }
    }

    #endregion
}
