﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

[RequireComponent (typeof(AudioSource))]

public class VideoPlay : MonoBehaviour {

    public RawImage _rawImage;

    public VideoPlayer _videoPlayer;
    public VideoSource _videoSource;
    public GameObject _background;

    string _videoURL;


    private AudioSource _audioSource;

    private void Awake()
    {
        //_rawImage = gameObject.GetComponent<RawImage>();
        //        _videoURL = Application.dataPath + "/Resources/Movies/TestMovie.mp4";

        _background.SetActive(false);
        //StartCoroutine(PlayVideo());        
    }

    void Start () {
        
	}

    public void OpenVideo(string url)
    {
        //_videoURL = Application.dataPath + "/Resources/Movies/TestMovie.mp4";
        _videoURL = url;
        if (!_rawImage.gameObject.activeSelf)
        {
            _rawImage.gameObject.SetActive(true);
            _background.SetActive(true);
        }
            
        StartCoroutine(PlayVideo());
    }

    public void CloseVideo()
    {
        OnclickClose();
    }

    IEnumerator PlayVideo()
    {
        _videoPlayer = gameObject.AddComponent<VideoPlayer>();
        _audioSource = gameObject.AddComponent<AudioSource>();

        _videoPlayer.playOnAwake = false;
        _audioSource.playOnAwake = false;

        _audioSource.Pause();

        _videoPlayer.source = VideoSource.Url;
        _videoPlayer.url = _videoURL;

        _videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

        _videoPlayer.EnableAudioTrack(0, true);
        _videoPlayer.SetTargetAudioSource(0, _audioSource);

        _videoPlayer.Prepare();

        WaitForSeconds waitTime = new WaitForSeconds(1);

        while(!_videoPlayer.isPrepared)
        {
            Debug.Log("Preparing Video");

            yield return waitTime;

            break;
        }

        Debug.Log("Done preparing Video");

        _rawImage.texture = _videoPlayer.texture;

        //video play
        _videoPlayer.Play();
        //audio play
        _audioSource.Play();

        while(_videoPlayer.isPlaying)
        {
            Debug.LogWarning("Video time : " + Mathf.FloorToInt((float)_videoPlayer.time));
            yield return null;
        }
    }


    // temp

    public void OnclickClose()
    {
        _videoPlayer.Stop();
        _audioSource.Stop();

        Destroy(_audioSource);
        Destroy(_videoPlayer);        

        _rawImage.gameObject.SetActive(false);
        _background.SetActive(false);
    }
	
}
