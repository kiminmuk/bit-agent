﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonController : MonoBehaviour {

    #region [Singleton]
    private static ButtonController _instance;

    public static ButtonController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType(typeof(ButtonController)) as ButtonController;
                if (_instance == null)
                {
                    Debug.Log("Instance Call Error");
                }
            }

            return _instance;
        }
    }

    #endregion

    #region [변수]

    public SceneController _scene_controller;

    public delegate void delfuc();

    #endregion

    public void Awake()
    {
        _scene_controller = null;
    }

    public void Init()
    {
        
    }

    public void ButtonEvent(eButtonEvent ebutton, UI_Button button)
    {
        switch(ebutton)
        {
            case eButtonEvent.None:
                break;
            case eButtonEvent.Detail:
                CallOpenDatialInfo(button._gameData);
                break;
            case eButtonEvent.Video:
                CallVideoPlay(button._gameData.VideoURL);
                break;
            case eButtonEvent.Menu:                
                CallSceneChange(button.SceneKey);
                break;
            case eButtonEvent.Back:
                CallBackButton();
                break;
            case eButtonEvent.Play:
                break;                
        }
    }

    public void CallVideoPlay(string videoURL)
    {
        _scene_controller.PlayVideo(videoURL);
    }

    public void CallSceneChange(string key)
    {
        _scene_controller.SceneChange(key);
    }

    public void CallOpenDatialInfo(GameData data)
    {
        GameDataManager.Instance.CurrentData = data;
        _scene_controller.SceneChange(data);
    }

    public void CallBackButton()
    {
        _scene_controller.BackPrevScene();
    }

    public void CallGamePlay(string gamePath)
    {
        KUtill.ProcessStart(gamePath);
    }


}
