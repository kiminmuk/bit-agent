﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneController : MonoBehaviour {

    [SerializeField]
    public Dictionary<string, GameObject> _scene_list;

    //public ButtonController _buttonController;

    public GameDataManager _gameDataManger;

    public Transform _canvas;
    public VideoPlay _video;
    public Button _home;

    [SerializeField]
    GameObject _current_scene;
    [SerializeField]
    GameObject _prevScene;

    public string _mainScene;

    //public GameObject CurrentScene
    //{
    //    set { }
    //    get { }
    //}

    public void Awake()
    {
        _scene_list = new Dictionary<string, GameObject>();
        //Init();
    }

    #region [Init]

    public void Init(string mainscene)
    {
        ////_gameDataManger = 
        _canvas = transform.parent;

        _home = KUtill.CreateGameObject("Prefabs/Scene/HomeButton", _canvas).GetComponent<Button>();
        _video = KUtill.CreateGameObject("Prefabs/Scene/Video", _canvas).GetComponent<VideoPlay>();
        
        _mainScene = mainscene;

        CreateScene();
        
    }

    #endregion 


    #region [Function]

    private void CreateScene()
    {
        var path_list = KUtill.GetResourcesPath("Prefabs/Window");

        for (int i = 0; i < path_list.Length; i++)
        {
            
            var prefab = KUtill.CreateGameObject(path_list[i],transform);
            prefab.SetActive(false);
            prefab.GetComponent<SceneBase>().Init(this);

            _scene_list.Add(prefab.name, prefab);
        }

        SetMainScene();
    }

    private void SetMainScene()
    {
        SceneChange(_mainScene);
    }


    // Scene change
    public void SceneChange(string key)
    {
        ChangeScene(key);
    }

    // 게임 상세 정보일때만 호출
    public void SceneChange(GameData data)
    {
        if (CheckSceneList("Detail"))
        {
            var scene = _scene_list["Detail"];

            if (_current_scene != scene)
            {
                _prevScene = _current_scene;
                _current_scene = scene;

                if (_prevScene != null)
                    _prevScene.SetActive(false);
                
                _current_scene.GetComponent<Detail_window>().OpenDetailWindow(data);
                //_current_scene.SetActive(true);
            }
        }
    }

    private void ChangeScene(string key)
    {       
        if(CheckSceneList(key))
        {
            var scene = _scene_list[key];

            if(_current_scene != scene)
            {                
                _prevScene =_current_scene;
                _current_scene = scene;

                if(_prevScene != null)
                    _prevScene.SetActive(false);

                _current_scene.SetActive(true);
            }            
        }
    }

    public void BackPrevScene()
    {
        if (_prevScene == null)
            return;

        _current_scene.SetActive(false);
        _prevScene.SetActive(true);

        KUtill.Sort(ref _current_scene, ref _prevScene);        
    }

    public void PlayVideo(string url)
    {
        if (_video != null)
        {
            _video.gameObject.SetActive(true);
            _video.OpenVideo(url);
        }
        else
        {
            _video = _canvas.Find("Video").GetComponent<VideoPlay>();
            _video.gameObject.SetActive(true);
            _video.OpenVideo(url);
        }
        //_video.OpenVideo(url);
    }

    public void CloseVideo()
    {
        _video.CloseVideo();
    }

    // Search Scene
    public GameObject SearchScene(string key)
    {
        var scene = new GameObject();

        return _scene_list.TryGetValue(key, out scene) ? scene : null;
    }

    private bool CheckSceneList(string key)
    {
        return _scene_list.ContainsKey(key);
    }

    


    #endregion
}
