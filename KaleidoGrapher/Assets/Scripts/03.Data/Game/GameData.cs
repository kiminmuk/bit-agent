﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData : MonoBehaviour {

    #region [Variable]

    private string _gameName;
    private string _peapleName;
    private string _firstMin;
    private string _lastMin;
    private string _detailText;
    private string _exeRoot;
    private string _imgName;
    private string _imgURL;
    private string _videoURL;

    public int _categoryCount;
    //public Sprite

    #endregion

    #region [Property]   

    public string GameName
    {
        set { _gameName = value; }
        get { return _gameName; }
    }

    public string PeapleName
    {
        set { _peapleName = value; }
        get { return _peapleName; }
    }

    public string DetailText
    {
        set { _detailText = value; }
        get { return _detailText; }
    }

    public string GamePath
    {
        set { _exeRoot = value; }
        get { return _exeRoot; }
    }

    public string ImageName
    {
        set { _imgName = value; }
        get { return _imgName; }
    }

    public string ImageURL
    {
        set { _imgURL = value; }
        get { return _imgURL; }
    }

    public string VideoURL
    {
        set { _videoURL = value; }
        get { return _videoURL; }
    }

    public int Category
    {
        set { _categoryCount = value; }
        get { return _categoryCount; }
    }



    #endregion

    #region [Funtion]

    // Use this for initialization
    void Start()
    {
        //StartCoroutine(ImageDownload());
        //var msprite = gameObject.GetComponent<Image>().sprite;
        //_imgName = msprite.name;
    }

    public void PlayMovie()
    {
        //Application.OpenURL("http://13.230.159.206:8000/");
        Application.OpenURL(" https://youtu.be/JUURZPqGZ1s");
    }

    public void OpenRootExe()
    {
        //System.Diagnostics.Process.Start("C:/Program Files/Starcraft/StarCraft.exe");
        KUtill.ProcessStart(_exeRoot);
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnButtonClick()
    {
        OpenRootExe();
    }

    // Game Data Setup                  (지금은 가라데이타)
    public void SetGameData(GameData info)
    {
        _gameName = info._gameName;
        _exeRoot = info._exeRoot;
        _categoryCount = info._categoryCount;
        _detailText = info._detailText;
        _imgName = info._imgName;
        _imgURL = info._imgURL;
        _videoURL = info._videoURL;
        _firstMin = info._firstMin;
        _lastMin = info._lastMin;
    }

    #endregion
}
